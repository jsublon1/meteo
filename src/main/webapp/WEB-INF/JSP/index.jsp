<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Meteo</title>
	<link rel='stylesheet' type='text/css' href='CSS/style.css' />
	</head>
<body>
	<h1>M�t�o by Julien</h1>
	<form id='formID'>
		<label for='ville'>Ville : <input type='text' id='ville' name='ville' /></label>
		<input type='submit' name='btn' />
	</form>
	<!-- Mise en forme pour la page sans erreur et ville -->
	<c:if test="${empty erreur && empty temps}">
		<script>
			document.getElementById("formID").style.borderBottomLeftRadius='20px';
			document.getElementById("formID").style.borderBottomRightRadius='20px';
		</script>	
	</c:if>
	
	<c:if test="${not empty erreur || not empty temps}">
		<div id='meteo'>
			<c:if test="${not empty erreur}">
				<p id='erreur'><c:out value='${erreur}'/></p>
			</c:if>
			<c:if test="${not empty temps}">
				<h2><c:out value="${ville }" /></h2>
				<img id='icon' src='<c:out value="${temps.image}" />' alt='iconTemps'>
				<p id='temps'><c:out value='${temps.description}'/></p>
				<p>Temp�rature : <c:out value='${temps.temperature}'/></p>
				<p>Vent : <c:out value='${temps.vent}'/></p>
			</c:if>
	
		</div>
	</c:if>
	
	
	<!-- <script>
		const appId = '6d83d123f04b771d156d71398f50b968';
		const city = '<c:out value="${ville}"/>';
		const url = 'https://api.openweathermap.org/data/2.5/weather?q='
						+city
						+',fr&lang=fr&units=metric&appid=' 
						+ appId;
		async function getMeteo(){
			var response = await fetch(url);
			var data = await response.json();
			var icon = data.weather[0].icon;
			var temps = data.weather[0].description;
			iconURL = "http://openweathermap.org/img/wn/"+icon+"@4x.png";
			document.getElementById('icon').src = iconURL;
			document.getElementById('temps').textContent = "temps "+temps;
		};
		
		getMeteo();
		
		
				
	</script> -->
</body>
</html>