package beans;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Meteo {

	public Meteo() {

	}

	public static Temps getWeather(String ville)
			throws UnsupportedEncodingException, IOException, ParseException {

		final String URL_PATTERN = "https://api.openweathermap.org/data/2.5/weather?q=%s,fr&lang=fr&units=metric&appid=%s";
		String apiId = "6d83d123f04b771d156d71398f50b968";
		String adresse = String.format(URL_PATTERN, ville, apiId);
		System.out.println(adresse);

		//Requete vers l'api
		URL url = new URL(adresse.toString());
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("Content-Type", "application/json");
		con.setRequestProperty("Accept", "application/json");
		con.setDoOutput(true);
		//récupération du flux
		InputStream inputReponse = con.getInputStream();
		//création d'un objet JSON
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = (JSONObject) jsonParser
				.parse(new InputStreamReader(inputReponse, "UTF-8"));

		//récupération du temps et de l'icone
		JSONArray tempsArray = (JSONArray) jsonObject.get("weather");
		JSONObject tempsJson = (JSONObject) tempsArray.get(0);
		//récupération de la température
		JSONObject mainJson = (JSONObject) jsonObject.get("main");
		String temperature = String.valueOf(
				Math.round(Float.parseFloat(mainJson.get("temp").toString())));
		//récupération de la vitesse du vent (converti de m/s en km/h
		JSONObject ventJson = (JSONObject) jsonObject.get("wind");
		String vitesseVent = String.valueOf(
				Math.round(3.6*Float.parseFloat(ventJson.get("speed").toString())));
		Temps temps = new Temps(tempsJson.get("description").toString(),
				tempsJson.get("icon").toString(), temperature, vitesseVent);

		return temps;

	}

}
