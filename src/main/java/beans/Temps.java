package beans;

public class Temps {

	String description;
	String image;
	String temperature;
	String vent;

	public Temps(String des, String img, String temp, String vent) {
		this.setDescription(des);
		this.setImage(img);
		this.setTemperature(temp);
		this.setVent(vent);
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the imgane
	 */
	public String getImage() {
		return this.image;
	}

	/**
	 * @param imgane the imgane to set
	 */
	public void setImage(String	 image) {
		this.image = String.format("http://openweathermap.org/img/wn/%s@4x.png", image);
	}
	
	/**
	 * @return the temperature
	 */
	public String getTemperature() {
		return temperature;
	}

	/**
	 * @param temperature the temperature to set
	 */
	public void setTemperature(String temperature) {
		this.temperature = temperature + " °C";
	}

	/**
	 * @return the vent
	 */
	public String getVent() {
		return vent;
	}

	/**
	 * @param vent the vent to set
	 */
	public void setVent(String vent) {
		this.vent = vent + " km/h";
	}
	
}
